---
layout: handbook-page-toc
title: "Contracts & International Expansion"
description: "GitLab's Employment Solutions."
---

## On this page

{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

This page has been split into two different handbook pages, for more information, please follow the links below.

## [Contracts, Probation Periods & PIAA](/handbook/people-group/contracts-probation-periods)

## [Employment Solutions - International Expansion & Relocations](/handbook/people-group/employment-solutions)
