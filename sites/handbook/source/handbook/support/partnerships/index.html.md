---
layout: handbook-page-toc
title: Partnerships
description: "Support specific information on partnership's workflows, automations and processes."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

The GitLab Partner Program enables many different types of partner.
Descriptions can be found on the respective pages for each partner type:

* [Open Partners](open.html)
* [Select Partners](select.html)
* [Alliance Partners](alliance.html)

Support provided to partners and their customers will vary depending on the
circumstances under which the ticket is raised. Some examples are:

1. An Open Partner raises a ticket on behalf of their end customer. They should
   raise the ticket under the Customer's account name and we will deliver
   support **based on the customer's subscription plan**. Support will
   correspond with the end customer directly. Please note that if the partner
   incorrectly types the customer's email address, the ticket may not be
   properly associated with the customer organization. This can be corrected
   with the following steps:

    1. Update `Organization Email (partners)` to use the correct email
    1. Change `Requester` by clicking `change` link under the ticket subject
    1. Submit the ticket to save your changes

1. A Select Partner creates and manages a ticket, acting as the go-between for
   the customer and GitLab Support. We will deliver
   **[Priority Support](https://about.gitlab.com/support/#priority-support)
   regardless of the customer's subscription plan**.
1. An Open partner raises a ticket, using the Open Partner form, while
   fulfilling a services engagement for their end customer. They should raise
   the ticket under the customer's account name and we will deliver support
   based on the customer's subscription plan. The 'To' email address will be
   the customer's. In the partner troubleshooting section of the form, the
   partner should make it known they would like to be CC'd on the ticket in
   question. If the customer email address requires correction to associate the
   ticket with the correct org, please see point 1, Steps 1-3, above.
1. A partner of any type is doing commercial work for a customer and raises a
   ticket. They should do so under the partner account against a subscription
   they have purchased for themselves. We will deliver support **based on the
   subscription the partner has purchased**.
1. An Open or Select partner is doing internal training, testing or knowledge
   building. They should raise a ticket under the partner account against their
   [NFR licences](https://about.gitlab.com/handbook/resellers/#nfr-programpolicy).

These examples are not exhaustive. If in doubt ask questions about the situation under which the ticket is being raised.
